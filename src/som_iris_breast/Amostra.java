package som_iris_breast;

public class Amostra {
    private int classe;
    private double[] entradas;

    public Amostra(int classe, double[] entradas) {
        this.classe = classe;
        this.entradas = entradas;
    }

    public int getClasse() {
        return classe;
    }

    public void setClasse(int classe) {
        this.classe = classe;
    }

    public double[] getEntradas() {
        return entradas;
    }

    public void setEntradas(double[] entradas) {
        this.entradas = entradas;
    }
    
    
    
}
