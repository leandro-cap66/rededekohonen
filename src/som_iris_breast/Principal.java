package som_iris_breast;

import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class Principal {

    public static void preencheArrayClasses(ArrayList<Double> classes, double saida) {
        if (classes.isEmpty()) {
            classes.add(saida);
        }
        if (!classes.contains(saida)) {
            classes.add(saida);
        }
    }

    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scan = new Scanner(System.in);
        int escolha = 0;
        File entrada = null;

        while (escolha == 0) {
            System.out.println("Selecione a base:");
            System.out.println("1-Base Iris");
            System.out.println("2-Base Breast Cancer Winsconsin");

            escolha = scan.nextInt();
            switch (escolha) {
                case 1:
                    entrada = new File("iris.txt");
                    break;
                case 2:
                    entrada = new File("breast.txt");
                    break;
                default:
                    escolha = 0;
                    break;
            }
        }
        Scanner in;
        try {
            in = new Scanner(entrada);
        } catch (FileNotFoundException evalor) {
            System.out.println("Arquivo não encontrado!Insira a base na mão mesmo:\n");
            in = new Scanner(System.in);
        }

        int numAmostras = Integer.parseInt(in.nextLine()); //Numero de amostras a serem consideradas
        int numEntradas = Integer.parseInt(in.nextLine());//Numero de elementos de uma amostra
        int numNeuronios = Integer.parseInt(in.nextLine());//Número de neurônios a serem utilizados
        int numEpocas = Integer.parseInt(in.nextLine());//Numero de epocas a serem consideradas
        double sigma0 = numNeuronios / 2;
        double ni0 = 0.1;
        double tau1 = numEpocas / Math.log(sigma0);
        double tau2 = numEpocas;
        double ni = 0;
        double sigma = 0;
        double[] erroGrafClassTreino = new double[numEpocas];
        double[] erroGrafClassTeste = new double[numEpocas];
        int maiorErroClassTreino = 0;
        int maiorErroClassTeste = 0;

        ArrayList<Amostra> matXList = new ArrayList<>();
        ArrayList<Double> classes = new ArrayList<>();

        if (entrada.getName().equals("breast.txt")) {
            for (int i = 0; i < numAmostras; i++) {
                String linhas = in.nextLine();
                String[] itens = linhas.split(",");
                double[] linha = new double[numEntradas];
                int j = 1;
                for (; j < numEntradas; j++) {
                    linha[j] = (Double.parseDouble(itens[j]));
                }
                preencheArrayClasses(classes, Double.parseDouble(itens[j]));
                Amostra amostra = new Amostra(classes.indexOf(Double.parseDouble(itens[j])), linha);
                matXList.add(amostra);
            }

        } else {
            for (int i = 0; i < numAmostras; i++) {
                String linhas = in.nextLine();
                String[] itens = linhas.split(",");
                double[] linha = new double[numEntradas];
                int j = 0;
                for (; j < numEntradas; j++) {
                    linha[j] = (Double.parseDouble(itens[j]));
                }
                preencheArrayClasses(classes, Double.parseDouble(itens[j]));
                Amostra amostra = new Amostra(classes.indexOf(Double.parseDouble(itens[j])), linha);
                matXList.add(amostra);
            }
        }

        int numClasses = classes.size();

        ArrayList<Amostra> linhasTreino = new ArrayList<>();
        ArrayList<Amostra> linhasTeste = new ArrayList<>();
        int numTreino = (int) (matXList.size() * 0.75); //Forçando os 3/4 da base para descobrir quantos elementos devem ser selecionados para o treino
        for (int i = 0; i < numTreino; i++) {//Selecionar aleatoriamente um número dentro do Array da classe, e adicionar este para o Array de linhas de treino
            linhasTreino.add(matXList.remove(rand.nextInt(matXList.size())));
        }
        linhasTeste.addAll(matXList);
        matXList.clear();

        RedeSOM mapaKonohen = new RedeSOM(numEntradas, numNeuronios);
        int erroClassTreino, erroClassTeste;
        for (int epoca = 0; epoca < numEpocas; epoca++) {
            //Calculando o sigma
            sigma = sigma0 * Math.exp(-epoca / tau1);
            //Calculando o ni
            ni = ni0 * Math.exp(-epoca / tau2);

            erroClassTreino = 0;
            erroClassTeste = 0;
            //For de amostras de treino
            for (int linhas = 0; linhas < linhasTreino.size(); linhas++) {
                //Pegando a linha de uma amostra
                double[] amostra = linhasTreino.get(linhas).getEntradas();
                mapaKonohen.treinar(ni, amostra, sigma);
            }
            int[][] mapaSoma = new int[numNeuronios][numClasses];

            //For de amostras de teste
            for (int linhas = 0; linhas < linhasTeste.size(); linhas++) {
                //Pegando a linha de uma amostra
                double[] amostra = linhasTeste.get(linhas).getEntradas();
                mapaKonohen.processoCompetitivo(amostra);//Calculando qual neurônio ganhou
                int i = mapaKonohen.indiceNeuronioGanhador;
                mapaSoma[i][linhasTeste.get(linhas).getClasse()]++;
            }

            int[] mapa = new int[numNeuronios];
            for (int i = 0; i < numNeuronios; i++) {//Linhas de MapaSoma
                int c = 0;//Assume que a 1 coluna tem maior valor
                for (int j = 1; j < numClasses; j++) {//Colunas
                    if (mapaSoma[i][c] < mapaSoma[i][j]) {
                        c = j;//Encontrando o maior valor da coluna
                    }
                }
                mapa[i] = c;
            }

            //Analisando generalização
            for (int linhas = 0; linhas < linhasTreino.size(); linhas++) {
                double[] amostra = linhasTreino.get(linhas).getEntradas();
                mapaKonohen.processoCompetitivo(amostra);//Calculando qual neurônio ganhou
                int i = mapaKonohen.indiceNeuronioGanhador;
                if (mapa[i] != linhasTreino.get(linhas).getClasse()) {
                    erroClassTreino++;
                }
            }

            //Analisando especialização
            for (int linhas = 0; linhas < linhasTeste.size(); linhas++) {
                double[] amostra = linhasTeste.get(linhas).getEntradas();
                mapaKonohen.processoCompetitivo(amostra);//Calculando qual neurônio ganhou
                int i = mapaKonohen.indiceNeuronioGanhador;
                if (mapa[i] != linhasTeste.get(linhas).getClasse()) {
                    erroClassTeste++;
                }
            }
            System.out.print("ÉPOCA: " + epoca + "\tERRO TREINO: " + erroClassTreino + "\tERRO TESTE: " + erroClassTeste + "\tMAPA: ");
            for (int i = 0; i < mapa.length; i++) {
                System.out.print(mapa[i] + " |");
            }
            System.out.println("");

            erroGrafClassTreino[epoca] = erroClassTreino;
            erroGrafClassTeste[epoca] = erroClassTeste;

            if (erroClassTeste > maiorErroClassTeste) {
                maiorErroClassTeste = erroClassTeste;

            }
            if (erroClassTreino > maiorErroClassTreino) {
                maiorErroClassTreino = erroClassTreino;
            }
        }
        for (int i = 0; i < erroGrafClassTreino.length; i++) {
            erroGrafClassTreino[i] /= numTreino;
        }
        for (int i = 0; i < erroGrafClassTeste.length; i++) {
            erroGrafClassTeste[i] /= numAmostras - numTreino ;
        }
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        for (int i = 0; i < erroGrafClassTreino.length; i++) {
            ds.addValue(erroGrafClassTreino[i], "Erro de Classificação Treino", i + "");
            ds.addValue(erroGrafClassTeste[i], "Erro de Classificação Teste", i + "");
        }
        String nome = null;
        String nomeArquivo = null;
        if (escolha == 1) {
            nome = "Rede SOM Base Iris - Erro de Classificação";
            nomeArquivo = "graficoIrisClassificacao.png";

        } else if (escolha == 2) {
            nome = "Rede SOM Base Breast Cancer Winsconsin - Erro de Classificação";
            nomeArquivo = "graficoBreastClassificacao.png";
        }

        JFreeChart grafico = ChartFactory.createLineChart(nome + "\n Número de neurônios: " + numNeuronios, "Época", "Erro", ds, PlotOrientation.VERTICAL, true, true, false);
        try (OutputStream arquivo = new FileOutputStream(nomeArquivo)) {
            ChartUtilities.writeChartAsPNG(arquivo, grafico, 1200, 800);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
