
package som_iris_breast;


public class RedeSOM {
    double [][] matrizDePesos;
    int indiceNeuronioGanhador;
    double[] hji;
    
    public RedeSOM(int numEntradas, int numNeuronios) {
        this.matrizDePesos = new double[numEntradas][numNeuronios];
        
//        Inicializando a matriz de pesos aleatorios
        for (int i = 0; i < numEntradas; i++) {
            for (int j = 0; j < numNeuronios; j++) {
//                Range: -0.3<=peso<=1.3
                matrizDePesos[i][j] = (Math.random()*1.6)-0.3;
            }
        }

//        matrizDePesos[0][0] = -0.3;
//        matrizDePesos[0][1] = 1.2;
//        matrizDePesos[0][2] = 0.7;
//
//        matrizDePesos[1][0] = 1.0;
//        matrizDePesos[1][1] = 0.5;
//        matrizDePesos[1][2] = 1.3;
        
    }
    
    private double distanciaEuclidianaAoQuadrado(double[] p1, double [] p2){
        //Calcula a distancia euclidiana ao quadrado
        //Entre dois pontos p1 e p2 em Rn
        if(p1.length != p2.length) return -1; //Pontos de dimensões diferentes
        int numDimensoes = p1.length;
        double distAoQuadrado = 0;
        for (int i = 0; i < numDimensoes; i++) {
            distAoQuadrado+=Math.pow(p1[i]-p2[i], 2);
        }
        return distAoQuadrado;
    }
    
    public void processoCompetitivo(double[] vetorEntradas){
        //Dados os vetores de entrada, é organizado o mapa de Kohonen para encontrar o vencedor
        double [] vetorColuna = new double[matrizDePesos.length];
        double menorDistanciaEuclidiana = Double.POSITIVE_INFINITY;
        for (int colunas = 0; colunas < matrizDePesos[0].length; colunas++) {
            for (int linhas = 0; linhas < matrizDePesos.length; linhas++) {
                vetorColuna[linhas] = matrizDePesos[linhas][colunas];
            }
            if(distanciaEuclidianaAoQuadrado(vetorEntradas, vetorColuna)<menorDistanciaEuclidiana){
                menorDistanciaEuclidiana = distanciaEuclidianaAoQuadrado(vetorEntradas, vetorColuna);
                this.indiceNeuronioGanhador = colunas;
            }
        }
    }
    
    public void processoCooperativo(double sigma){
        int numNeuronios = matrizDePesos[0].length;
        int i = this.indiceNeuronioGanhador;
        //dji = Distância lateral entre os neurônios
        int[] dji = new int[numNeuronios];
        for (int j = 0; j < dji.length; j++) {
            dji[j] = Math.abs(j-i);
        }
        //hij = Vizinhança topológica
        this.hji = new double[numNeuronios];
        for (int j = 0; j < this.hji.length; j++) {
            this.hji[j] = Math.pow(Math.E, -(dji[j])/(2*Math.pow(sigma, 2)) );
        }
    }
    
    public void processoAdaptativo(double ni, double[] vetorEntradas){
        for (int i = 0; i < matrizDePesos.length; i++) {
            for (int j = 0; j < matrizDePesos[0].length; j++) {
                this.matrizDePesos[i][j] = this.matrizDePesos[i][j]+(ni*this.hji[j]*(vetorEntradas[i]-this.matrizDePesos[i][j]));
            }
        }
    }
    public void treinar(double ni, double[] vetorEntradas, double sigma){
        processoCompetitivo(vetorEntradas);
        processoCooperativo(sigma);
        processoAdaptativo(ni, vetorEntradas);
    }
    
    
    
    
    
    
}
